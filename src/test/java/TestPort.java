
import hk.quantr.mxgraph.model.mxCell;
import hk.quantr.mxgraph.model.mxGeometry;
import hk.quantr.mxgraph.swing.mxGraphComponent;
import hk.quantr.mxgraph.util.mxConstants;
import hk.quantr.mxgraph.util.mxPoint;
import hk.quantr.mxgraph.util.mxRectangle;
import hk.quantr.mxgraph.view.mxEdgeStyle;
import hk.quantr.mxgraph.view.mxGraph;
import java.util.Map;
import javax.swing.JFrame;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestPort {

	private static final long serialVersionUID = -464235672367772404L;

	final int PORT_DIAMETER = 50;
	final int PORT_RADIUS = PORT_DIAMETER / 2;

	@Test
	public void test() {
		mxGraph graph = new mxGraph() {

			// Ports are not used as terminals for edges, they are
			// only used to compute the graphical connection point
			public boolean isPort(Object cell) {
				mxGeometry geo = getCellGeometry(cell);

				return (geo != null) ? geo.isRelative() : false;
			}

			// Implements a tooltip that shows the actual
			// source and target of an edge
			public String getToolTipForCell(Object cell) {
				if (model.isEdge(cell)) {
					return convertValueToString(model.getTerminal(cell, true)) + " -> "
							+ convertValueToString(model.getTerminal(cell, false));
				}

				return super.getToolTipForCell(cell);
			}

			// Removes the folding icon and disables any folding
			public boolean isCellFoldable(Object cell, boolean collapse) {
				return false;
			}
		};
		graph.setHtmlLabels(true);

		// Sets the default edge style
		Map<String, Object> style = graph.getStylesheet().getDefaultEdgeStyle();
		style.put(mxConstants.STYLE_EDGE, mxEdgeStyle.OrthConnector);

		Object parent = graph.getDefaultParent();

		graph.getModel().beginUpdate();
		try {
			mxCell v1 = (mxCell) graph.insertVertex(parent, null, "Hello", 50, 20, 100, 100, "strokeWidth=1;strokeColor=#BB3D00;fillColor=red;gradientColor=yellow;fontColor=red;");
			v1.setConnectable(false);
			mxGeometry geo = graph.getModel().getGeometry(v1);
			// The size of the rectangle when the minus sign is clicked
			geo.setAlternateBounds(new mxRectangle(20, 20, 100, 50));

			mxGeometry geo1 = new mxGeometry(0, 0.5, PORT_DIAMETER, PORT_DIAMETER);
			geo1.setOffset(new mxPoint(-PORT_RADIUS, -PORT_RADIUS));
			geo1.setRelative(true);
			mxCell port1 = new mxCell("fuck", geo1, "shape=rectangle;perimter=ellipsePerimeter");
			port1.setVertex(true);

			mxGeometry geo2 = new mxGeometry(1, 0.5, PORT_DIAMETER, PORT_DIAMETER);
			geo2.setOffset(new mxPoint(-PORT_RADIUS, -PORT_RADIUS));
			geo2.setRelative(true);
			mxCell port2 = new mxCell("you", geo2, "shape=rectangle;perimter=ellipsePerimeter;portConstraint=east");
			port2.setVertex(true);

			mxGeometry geo3 = new mxGeometry(1, 0.7, PORT_DIAMETER, PORT_DIAMETER);
			geo3.setOffset(new mxPoint(-PORT_RADIUS, -PORT_RADIUS));
			geo3.setRelative(true);
			mxCell port3 = new mxCell("you", geo3, "shape=rectangle;perimter=ellipsePerimeter");
			port3.setVertex(true);

			graph.addCell(port1, v1);
			graph.addCell(port2, v1);
			graph.addCell(port3, v1);

			Object v2 = graph.insertVertex(parent, null, "<html><b>My HTML Vertex</b><hr noshade size=''3''>'...\n"
					+ "              <table border=1 cellspacing=1 cellpadding=5>\n"
					+ "              <tr><td><b>Style</b></td><td><b>Value</b></td></tr>\n"
					+ "              <tr><td>rounded</td><td>1</td></tr>\n"
					+ "              <tr><td>fillColor</td><td>yellow</td></tr>\n"
					+ "              </table></html>", 100, 150, 200, 200, "fillColor=red;portConstraint=west");

			graph.insertEdge(parent, null, "Edge", port2, v2);
		} finally {
			graph.getModel().endUpdate();
		}

		mxGraphComponent graphComponent = new mxGraphComponent(graph);

		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(1000, 800);
		frame.getContentPane().add(graphComponent);
		frame.setVisible(true);
		graphComponent.setToolTips(true);
		while (true);
	}
}
