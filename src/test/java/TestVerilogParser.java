
import hk.quantr.netbeans.verilog.parser.MyVerilogParserListener;
import hk.quantr.verilogcompiler.antlr.VerilogLexer;
import hk.quantr.verilogcompiler.antlr.VerilogParser;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestVerilogParser {

	@Test
	public void test() throws FileNotFoundException, IOException {
		String content = IOUtils.toString(new FileReader("/Users/peter/workspace/quantr-i/constant.v"));
		VerilogLexer lexer = new VerilogLexer(CharStreams.fromString(content));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		VerilogParser parser = new VerilogParser(tokenStream);
		VerilogParser.Source_textContext context = parser.source_text();
		ParseTreeWalker walker = new ParseTreeWalker();
		MyVerilogParserListener listener = new MyVerilogParserListener(parser);
		walker.walk(listener, context);

//		VerilogLexer lexer = new VerilogLexer(CharStreams.fromString(content));
//		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
//		MyVerilogParserListener listener = new MyVerilogParserListener(parser);
//		VerilogParser parser = new VerilogParser(tokenStream);
//		parser.addParseListener(listener);
//		VerilogParser.Source_textContext context = parser.source_text();
	}
}
