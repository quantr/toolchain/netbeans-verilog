import com.formdev.flatlaf.FlatLightLaf;
import hk.quantr.netbeans.verilog.window.graph.GraphWindowTopComponent;
import java.awt.Font;
import java.io.File;
import javax.swing.JFrame;
import javax.swing.UIManager;
import org.antlr.v4.runtime.RuntimeMetaData;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestGraphTopComponent {

	@Test
	@SuppressWarnings("empty-statement")
	public void test() throws InterruptedException {
		if (!System.getProperty("os.name").equals("Mac OS X")) {
			try {
				UIManager.setLookAndFeel(new FlatLightLaf());
				UIManager.getLookAndFeelDefaults().put("defaultFont", new Font("Segoe", Font.PLAIN, 16));
			} catch (Exception ex) {
				System.err.println("Failed to initialize LaF");
			}
		}
		System.out.println("antlr version = " + RuntimeMetaData.VERSION);
		GraphWindowTopComponent c = new GraphWindowTopComponent();
		c.graphTopComponent1.loadAllVerilog(new File("c:\\workspace\\quantr-i\\src\\quantr_i.v"));
		JFrame frame = new JFrame();
		frame.setSize(1200, 800);
		frame.getContentPane().add(c);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		while (true) {
			Thread.sleep(500);
		}
	}

}
