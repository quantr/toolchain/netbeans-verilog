
import hk.quantr.verilogcompiler.antlr.VerilogLexer;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import org.antlr.v4.runtime.CharStreams;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestLexer {

    @Test
    public void test() throws IOException {
		for (int x = 0; x <= VerilogLexer.VOCABULARY.getMaxTokenType(); x++) {
			String name = VerilogLexer.VOCABULARY.getDisplayName(x);
			System.out.println(x + "\t" + name);
		}
		System.out.println("-----------------");
		int x = 0;
		for (String s : VerilogLexer.ruleNames) {
			System.out.println(x + " = " + s);
			x++;
		}
		System.out.println("-----------------");

		String str = "/* fuck */\n"
				+ "module sample();\n"
				+ "endmodule\n"
				+ "";
		InputStream stream = new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8));
		VerilogLexer lexer = new VerilogLexer(CharStreams.fromStream(stream, StandardCharsets.UTF_8));
		org.antlr.v4.runtime.Token token = lexer.nextToken();
		do {
			System.out.println("> " + token.getType() + "\t" + token.getText() + "\t" + token);
			token = lexer.nextToken();
		} while (token.getType() != -1);
    }
}
