package hk.quantr.netbeans;

import hk.quantr.netbeans.verilog.quantrprofiling.ProfilingData;
import hk.quantr.netbeans.verilog.quantrprofiling.QuantrProfiling;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.plaf.TextUI;
import javax.swing.text.BadLocationException;
import javax.swing.text.JTextComponent;
import javax.swing.text.Position;
import javax.swing.text.View;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.MutablePair;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.api.io.IOProvider;
import org.netbeans.api.io.InputOutput;
import org.netbeans.editor.BaseTextUI;
import org.netbeans.editor.Utilities;
import org.openide.util.Exceptions;

public class ModuleLib {

    public static boolean isDebug = false;
    static SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:s.S");
    static String themeName = UIManager.getLookAndFeel().getName();

    public static void log(String str) {
        if (isDebug) {
            InputOutput io = IOProvider.getDefault().getIO("Verilog", false);
            io.getOut().println(sdf.format(new Date()) + " - " + str);
        } else {
            System.err.println(str);
        }
    }

    public static void log(Object obj) {
        if (obj != null) {
            log(obj.toString());
        }
    }

    public static String printException(Exception ex) {
        StringWriter errors = new StringWriter();
        ex.printStackTrace(new PrintWriter(errors));
        return errors.toString();
    }

    public static boolean isMac() {
        if (System.getProperty("os.name").toLowerCase().contains("mac")) {
            return true;
        } else {
            return false;
        }
    }

    public static void print(JComponent component, String str) {
        ModuleLib.log("+++ " + str + component);
        for (int x = 0; x < component.getComponentCount(); x++) {
            if (component.getComponent(x) instanceof JComponent) {
                print((JComponent) component.getComponent(x), str + "\t");
            }
        }
    }

    public static JComponent getJComponent(JComponent component, Class c, String str) {
        //ModuleLib.log("---- " + str + component.getClass() + " == " + c + " > " + (component.getClass() == c));
        if (component.getClass() == c) {
            return component;
        }
        for (int x = 0; x < component.getComponentCount(); x++) {
            if (component.getComponent(x) instanceof JComponent) {
                JComponent temp = getJComponent((JComponent) component.getComponent(x), c, str + "\t");
                if (temp != null) {
                    return temp;
                }
            }
        }
        return null;
    }

    public static boolean isDarkTheme() {
        JTextComponent jTextComponent = EditorRegistry.lastFocusedComponent();
        if (jTextComponent == null) {
            return false;
        } else {
            if (isColorDark(jTextComponent.getBackground())) {
                return true;
            } else {
                return false;
            }
        }
//		return true;
    }

    public static boolean isColorDark(Color color) {
        float[] hsb = Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), null);
        float brightness = hsb[2];
        return brightness < 0.5;
    }

    public static File getRealFile(File file) {
        File realFile;
        if (file.getPath().startsWith("\\mnt\\c")) {
            realFile = new File(file.getPath().replace("\\mnt\\c", "c:\\"));
        } else {
            realFile = file;
        }
        return realFile;
    }

    public static HashMap<Integer, ImmutablePair<Integer, Double>> getLineY(Graphics g, JTextComponent textComponent) {
        HashMap<Integer, ImmutablePair<Integer, Double>> map = new HashMap<>();

        View rootView = Utilities.getDocumentView(textComponent);
        TextUI textUI = textComponent.getUI();
        Rectangle clip = g.getClipBounds();

        ArrayList<ProfilingData> data = QuantrProfiling.getInstance().currentData();
        if (data != null) {
            try {
                int startPos;
                if (textUI instanceof BaseTextUI) {
                    startPos = ((BaseTextUI) textUI).getPosFromY(clip.y);
                } else {
                    startPos = textUI.modelToView(textComponent, textUI.viewToModel(textComponent, new Point(0, clip.y))).y;
                }

                int startViewIndex = rootView.getViewIndex(startPos, Position.Bias.Forward);
                int rootViewCount = rootView.getViewCount();
                if (startViewIndex >= 0 && startViewIndex < rootViewCount) {
                    int clipEndY = clip.y + clip.height;

                    for (int i = 0; i < rootViewCount; i++) {
//						System.out.println("i=" + i);
                        View view = rootView.getView(i);
                        if (view == null) {
                            break;
                        }

                        // for zoom-in or zoom-out
                        Rectangle rec1 = textComponent.modelToView(view.getStartOffset());
                        Rectangle rec2 = textComponent.modelToView(view.getEndOffset() - 1);
                        if (rec2 == null || rec1 == null) {
                            break;
                        }

                        int y = rec1.y;
                        double lineHeight = (rec2.getY() + rec2.getHeight() - rec1.getY());
                        map.put(i, new ImmutablePair<Integer, Double>(y - (int) lineHeight, lineHeight));
                        y += lineHeight;
                        if (y >= clipEndY) {
                            break;
                        }
                    }
                }
            } catch (BadLocationException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
        return map;
    }

    public static Font getFontSize(Graphics g, Font font, int height) {
        for (int x = font.getSize(); x >= 1; x--) {
            FontMetrics metrics = g.getFontMetrics(font.deriveFont(x));
            if (metrics.getHeight() <= height) {
                return font.deriveFont(x);
            }
        }
        return font;
    }

}
