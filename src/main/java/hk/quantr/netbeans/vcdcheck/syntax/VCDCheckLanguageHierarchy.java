package hk.quantr.netbeans.vcdcheck.syntax;

import hk.quantr.vcdcheck.antlr.VCDCheckLexer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.netbeans.spi.lexer.LanguageHierarchy;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;
import org.openide.util.Exceptions;

public class VCDCheckLanguageHierarchy extends LanguageHierarchy<VCDCheckTokenId> {

	private final static List<VCDCheckTokenId> tokens = new ArrayList<VCDCheckTokenId>();
	private final static Map<Integer, VCDCheckTokenId> idToTokens = new HashMap<Integer, VCDCheckTokenId>();

	static {
		int index = 0;
		for (int x = 0; x <= VCDCheckLexer.VOCABULARY.getMaxTokenType(); x++) {
			String name = VCDCheckLexer.VOCABULARY.getDisplayName(x);
			if (name == null) {
				name = "INVALID" + x;
			}
			name = name.replaceAll("'", "");
			VCDCheckTokenId token = new VCDCheckTokenId(name, name, index);
			tokens.add(token);
			idToTokens.put(index, token);
			index++;
		}

		for (int x = 0; x <= VCDCheckLexer.VOCABULARY.getMaxTokenType(); x++) {
			String name = VCDCheckLexer.VOCABULARY.getDisplayName(x);
			if (name == null) {
				name = "INVALID" + x;
			}
			name = name.replaceAll("'", "");

			VCDCheckTokenId token = new VCDCheckTokenId("DARK_" + name, "DARK_" + name, index);
			tokens.add(token);
			idToTokens.put(index, token);
			index++;
		}
	}

	public static synchronized VCDCheckTokenId getToken(int id) {
		return idToTokens.get(id);
	}

	public static synchronized VCDCheckTokenId getToken(String name) {
		for (Map.Entry<Integer, VCDCheckTokenId> entry : idToTokens.entrySet()) {
			if (entry.getValue().name().equals(name)) {
				return entry.getValue();
			}
		}
		return null;
	}

	@Override
	protected synchronized Collection<VCDCheckTokenId> createTokenIds() {
		return tokens;
	}

	@Override
	protected synchronized Lexer<VCDCheckTokenId> createLexer(LexerRestartInfo<VCDCheckTokenId> info) {
		try {
			return new VCDCheckEditorLexer(info);
		} catch (IOException ex) {
			Exceptions.printStackTrace(ex);
			return null;
		}
	}

	@Override
	protected String mimeType() {
		return "text/x-vc";
	}
}
