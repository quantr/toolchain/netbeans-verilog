package hk.quantr.netbeans.vcdcheck.syntax;

import hk.quantr.netbeans.ModuleLib;
import hk.quantr.vcdcheck.antlr.VCDCheckLexer;
import java.io.IOException;
import org.netbeans.api.lexer.Token;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;

public class VCDCheckEditorLexer implements Lexer<VCDCheckTokenId> {

	private LexerRestartInfo<VCDCheckTokenId> info;
	private VCDCheckLexer lexer;

	public VCDCheckEditorLexer(LexerRestartInfo<VCDCheckTokenId> info) throws IOException {
		this.info = info;
		VCDCheckCharStream charStream = new VCDCheckCharStream(info.input(), "VCDCheckEditor");
		lexer = new VCDCheckLexer(charStream);
	}

	@Override
	public Token<VCDCheckTokenId> nextToken() {
		org.antlr.v4.runtime.Token token = lexer.nextToken();
		if (token.getType() != VCDCheckLexer.EOF) {
			String tokenName = VCDCheckLanguageHierarchy.getToken(token.getType()).name().replaceAll("'", "");
			//System.out.println("tokenName=" + tokenName);

			if (ModuleLib.isDarkTheme()) {
				VCDCheckTokenId darkTokenId = VCDCheckLanguageHierarchy.getToken("DARK_" + tokenName);
				return info.tokenFactory().createToken(darkTokenId);
			} else {
				VCDCheckTokenId tokenId = VCDCheckLanguageHierarchy.getToken(tokenName);
				return info.tokenFactory().createToken(tokenId);
			}
		}
		System.out.println("shit=" + token.getType());
//		return info.tokenFactory().createToken(VCDCheckLanguageHierarchy.getToken("WS"));
		return null;
	}

	@Override
	public Object state() {
		return null;
	}

	@Override
	public void release() {
	}

}
