///*
// * Copyright 2022 Peter <peter@quantr.hk>.
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *      http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//package hk.quantr.netbeans.verilog.highlight;
//
//import javax.swing.text.Document;
//import org.netbeans.api.editor.mimelookup.MimeRegistration;
//import org.netbeans.api.editor.mimelookup.MimeRegistrations;
//import org.netbeans.spi.editor.highlighting.HighlightsLayer;
//import org.netbeans.spi.editor.highlighting.HighlightsLayerFactory;
//import org.netbeans.spi.editor.highlighting.ZOrder;
//
///**
// *
// * @author Peter <peter@quantr.hk>
// */
//@MimeRegistrations({
//	@MimeRegistration(mimeType = "text/x-verilog", service = HighlightsLayerFactory.class)
//})
//public class VerilogHighlightsLayerFactory implements HighlightsLayerFactory {
//
//	public static VerilogHighlighter getMarkOccurrencesHighlighter(Document doc) {
//		VerilogHighlighter highlighter = (VerilogHighlighter) doc.getProperty(VerilogHighlighter.class);
//		if (highlighter == null) {
//			doc.putProperty(VerilogHighlighter.class, highlighter = new VerilogHighlighter(doc));
//		}
//		return highlighter;
//	}
//
//	@Override
//	public HighlightsLayer[] createLayers(Context context) {
//		return new HighlightsLayer[]{
//			HighlightsLayer.create(VerilogHighlighter.class.getName(), ZOrder.CARET_RACK.forPosition(2000), true, getMarkOccurrencesHighlighter(context.getDocument()).getHighlightsBag())
//		};
//	}
//}
