/*
 * Copyright 2022 peter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.netbeans.verilog.codecompletion;

import java.net.URL;
import javax.swing.Action;
import org.netbeans.spi.editor.completion.CompletionDocumentation;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class VerilogCompletionDocumentation implements CompletionDocumentation {

	private VerilogCompletionItem item;

	public VerilogCompletionDocumentation(VerilogCompletionItem item) {
		this.item = item;
	}

	@Override
	public String getText() {
		return "Information about " + item.text;
	}

	@Override
	public URL getURL() {
		return null;
	}

	@Override
	public CompletionDocumentation resolveLink(String string) {
		return null;
	}

	@Override
	public Action getGotoSourceAction() {
		return null;
	}
}
