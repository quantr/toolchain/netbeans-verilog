/*
 * Copyright 2022 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.netbeans.verilog.codecompletion;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;
import javax.swing.UIManager;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.StyledDocument;
import org.netbeans.api.editor.completion.Completion;
import org.netbeans.spi.editor.completion.CompletionItem;
import org.netbeans.spi.editor.completion.CompletionResultSet;
import org.netbeans.spi.editor.completion.CompletionTask;
import org.netbeans.spi.editor.completion.support.AsyncCompletionQuery;
import org.netbeans.spi.editor.completion.support.AsyncCompletionTask;
import org.netbeans.spi.editor.completion.support.CompletionUtilities;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class VerilogCompletionItem implements CompletionItem {

	public final String text;
	private final ImageIcon fieldIcon = new ImageIcon(ImageUtilities.loadImage("hk/quantr/netbeans/verilog/verilog.png"));
	private final Color fieldColor = (Color) UIManager.get("List.selectionBackground");
	private final int caretOffset;

	public VerilogCompletionItem(String text, int caretOffset) {
		this.text = text;
		this.caretOffset = caretOffset;
	}

	@Override
	public int getPreferredWidth(Graphics graphics, Font font) {
		return CompletionUtilities.getPreferredWidth(text, null, graphics, font);
	}

	@Override
	public void render(Graphics g, Font defaultFont, Color defaultColor, Color backgroundColor, int width, int height, boolean selected) {
		CompletionUtilities.renderHtml(fieldIcon, text, null, g, defaultFont, (selected ? Color.white : fieldColor), width, height, selected);
	}

	@Override
	public CharSequence getSortText() {
		return text;
	}

	@Override
	public CharSequence getInsertPrefix() {
		return text;
	}

	@Override
	public void defaultAction(JTextComponent jtc) {
		try {
			StyledDocument doc = (StyledDocument) jtc.getDocument();
			if (text.equals("always")) {
				doc.insertString(caretOffset, "always @(a or b) begin\n\n\nend\n", null);
			}
			Completion.get().hideAll();
		} catch (BadLocationException ex) {
			Exceptions.printStackTrace(ex);
		}
	}

	@Override
	public void processKeyEvent(KeyEvent ke) {
	}

	@Override
	public CompletionTask createDocumentationTask() {
		return new AsyncCompletionTask(new AsyncCompletionQuery() {
			@Override
			protected void query(CompletionResultSet completionResultSet, Document document, int i) {
				completionResultSet.setDocumentation(new VerilogCompletionDocumentation(VerilogCompletionItem.this));
				completionResultSet.finish();
			}
		});
	}

	@Override
	public CompletionTask createToolTipTask() {
		throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
	}

	@Override
	public boolean instantSubstitution(JTextComponent jtc) {
		return false;
	}

	@Override
	public int getSortPriority() {
		return 0;
	}
}
