// License : Apache License Version 2.0  https://www.apache.org/licenses/LICENSE-2.0
package hk.quantr.netbeans.verilog.sourceformatter;

import hk.quantr.netbeans.ModuleLib;
import hk.quantr.netbeans.verilog.VerilogDataObject;
import hk.quantr.javalib.CommonLib;
import java.io.IOException;
import javax.swing.text.BadLocationException;
import org.netbeans.modules.editor.indent.spi.Context;
import org.netbeans.modules.editor.indent.spi.ExtraLock;
import org.netbeans.modules.editor.indent.spi.ReformatTask;
import org.netbeans.modules.editor.indent.spi.ReformatTask.Factory;
import org.openide.util.Exceptions;
import org.openide.windows.TopComponent;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class SourceReformatFactory implements Factory {

	@Override
	public ReformatTask createTask(Context cntxt) {
		return new SourceReformat(cntxt);
	}

	public class SourceReformat implements ReformatTask {

		Context ctx;

		public SourceReformat(Context c) {
			this.ctx = c;
		}

		@Override
		public void reformat() throws BadLocationException {
			try {
				TopComponent activeTC = TopComponent.getRegistry().getActivated();
				VerilogDataObject dataObject = activeTC.getLookup().lookup(VerilogDataObject.class);
				ModuleLib.log(dataObject);
				String path = dataObject.getPrimaryFile().getPath();
				String str = CommonLib.runCommand("/opt/verilog-format/verilog-format -p -f " + path);
				System.out.println(str);

				String toFormat = ctx.document().getText(ctx.startOffset(), ctx.endOffset());
				ctx.document().remove(ctx.startOffset(), ctx.endOffset() - ctx.startOffset());
				ctx.document().insertString(ctx.startOffset(), str, null);
			} catch (IOException ex) {
				Exceptions.printStackTrace(ex);
			} catch (InterruptedException ex) {
				Exceptions.printStackTrace(ex);
			} catch (Exception ex) {
				Exceptions.printStackTrace(ex);
			}
		}

		@Override
		public ExtraLock reformatLock() {
			return null;
		}
	}
}
