/*
 * Copyright 2021 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.netbeans.verilog.projecttype;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import org.netbeans.api.project.Project;
import org.netbeans.spi.project.CopyOperationImplementation;
import org.openide.filesystems.FileObject;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class CopyOperation implements CopyOperationImplementation {

	private final VerilogProject project;
	private final FileObject projectDir;

	public CopyOperation(VerilogProject project) {
		this.project = project;
		this.projectDir = project.getProjectDirectory();
	}

	public List<FileObject> getMetadataFiles() {
		return Collections.EMPTY_LIST;
	}

	public List<FileObject> getDataFiles() {
		return Collections.EMPTY_LIST;
	}

	public void notifyCopying() throws IOException {
	}

	public void notifyCopied(Project arg0, File arg1, String arg2) throws IOException {
	}
}
