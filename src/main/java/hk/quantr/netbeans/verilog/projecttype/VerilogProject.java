package hk.quantr.netbeans.verilog.projecttype;

import org.netbeans.api.project.Project;
import org.netbeans.spi.project.ProjectState;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class VerilogProject implements Project {

	private final FileObject projectDir;
	private final ProjectState state;
	private Lookup lkp;

	public VerilogProject(FileObject projectDir, ProjectState state) {
		this.projectDir = projectDir;
		this.state = state;
	}

	@Override
	public FileObject getProjectDirectory() {
		return projectDir;
	}

	@Override
	public Lookup getLookup() {
		if (lkp == null) {
			lkp = Lookups.fixed(new Object[]{
				state,
				new ActionProviderImpl(this),
//				new DeleteOperation(),
//				new CopyOperation(this),
//				new RenameOperation(),
				new ProjectInfo(this),
				new VerilogProjectLogicalView(this),
				new VerilogProjectCustomizerProvider(this),});
		}
		return lkp;
	}

}
