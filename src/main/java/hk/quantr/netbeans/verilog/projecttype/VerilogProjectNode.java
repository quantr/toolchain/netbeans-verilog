/*
 * Copyright 2021 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.netbeans.verilog.projecttype;

import java.awt.Image;
import javax.swing.Action;
import org.netbeans.spi.project.ui.support.CommonProjectActions;
import org.netbeans.spi.project.ui.support.NodeFactorySupport;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class VerilogProjectNode extends FilterNode {

	final VerilogProject project;

	public VerilogProjectNode(Node node, VerilogProject project) throws DataObjectNotFoundException {
//		super(node);
//		super(NodeFactorySupport.createCompositeChildren(project, "Projects/verilog-project/Nodes"), Lookups.singleton(project));

		super(node, NodeFactorySupport.createCompositeChildren(
				project,
				"Projects/verilog-project/Nodes"), 
				new ProxyLookup(
						new Lookup[]{
							Lookups.singleton(project),
							node.getLookup()
						}));

		this.project = project;
	}

	@Override
	public Action[] getActions(boolean arg0) {
		Action[] nodeActions = new Action[7];
		nodeActions[0] = CommonProjectActions.newFileAction();
		nodeActions[1] = CommonProjectActions.copyProjectAction();
		nodeActions[2] = CommonProjectActions.deleteProjectAction();
		nodeActions[3] = CommonProjectActions.renameProjectAction();
		nodeActions[4] = CommonProjectActions.moveProjectAction();
		nodeActions[5] = CommonProjectActions.closeProjectAction();
		nodeActions[6] = CommonProjectActions.customizeProjectAction();
		return nodeActions;
	}

	@Override
	public Image getIcon(int type) {
		return ImageUtilities.loadImage("hk/quantr/netbeans/verilog/project.png");
	}

	@Override
	public Image getOpenedIcon(int type) {
		return getIcon(type);
	}

	@Override
	public String getDisplayName() {
		return project.getProjectDirectory().getName();
	}

}
