/*
 * Copyright 2021 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.netbeans.verilog.projecttype.settingpanel;

import javax.swing.JComponent;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ui.OpenProjects;
import org.netbeans.spi.project.ui.support.ProjectCustomizer;
import org.netbeans.spi.project.ui.support.ProjectCustomizer.Category;
import org.openide.util.Lookup;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TopLevelComponentProperties implements ProjectCustomizer.CompositeCategoryProvider {

	private static final String GENERAL = "General";

	@ProjectCustomizer.CompositeCategoryProvider.Registration(projectType = "verilog-project", position = 10)
	public static TopLevelComponentProperties createGeneral() {
		return new TopLevelComponentProperties();
	}

	@Override
	public Category createCategory(Lookup lkp) {
		return ProjectCustomizer.Category.create(
				GENERAL,
				"Top Level Module",
				null);
	}

	@Override
	public JComponent createComponent(Category category, Lookup lkp) {
		Project project = OpenProjects.getDefault().getOpenProjects()[0];
		return new TopLevelComponentPanel(category, project);
	}
}
