/*
 * Copyright 2021 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.netbeans.verilog.projecttype;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.swing.event.ChangeListener;
import org.netbeans.api.project.Project;
import org.netbeans.spi.project.ui.support.NodeFactory;
import org.netbeans.spi.project.ui.support.NodeList;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
@NodeFactory.Registration(projectType = "verilog-project", position = 10)
public class VerilogNodeFactory implements NodeFactory {

	@Override
	public NodeList<?> createNodes(Project project) {
		return new VerilogNodeList(project);
	}

	private class VerilogNodeList implements NodeList<Node> {

		Project project;

		public VerilogNodeList(Project project) {
			this.project = project;
		}

		@Override
		public List<Node> keys() {
			FileObject folder = project.getProjectDirectory();
			List<Node> result = new ArrayList<>();
			if (folder != null) {
				for (FileObject f : Stream.of(folder.getChildren())
						.filter(e -> e.isFolder() && !e.getName().startsWith("."))
						.sorted(Comparator.comparing(FileObject::getName))
						.collect(Collectors.toList())) {
					try {
						System.out.println(f.getName());
						result.add(DataObject.find(f).getNodeDelegate());
					} catch (DataObjectNotFoundException ex) {
						Exceptions.printStackTrace(ex);
					}
				}
				for (FileObject f : Stream.of(folder.getChildren())
						.filter(e -> !e.isFolder() && !e.getName().startsWith(".") && !e.getName().equals("verilog.hk"))
						.sorted(Comparator.comparing(FileObject::getName))
						.collect(Collectors.toList())) {
					try {
						result.add(DataObject.find(f).getNodeDelegate());
					} catch (DataObjectNotFoundException ex) {
						Exceptions.printStackTrace(ex);
					}
				}
			}
			return result;
		}

		@Override
		public Node node(Node node) {
			return new FilterNode(node);
		}

		@Override
		public void addNotify() {
		}

		@Override
		public void removeNotify() {
		}

		@Override
		public void addChangeListener(ChangeListener cl) {
		}

		@Override
		public void removeChangeListener(ChangeListener cl) {
		}

	}
}
