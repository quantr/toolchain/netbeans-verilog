/*
 * Copyright 2021 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.netbeans.verilog.projecttype;

import org.netbeans.spi.project.ui.LogicalViewProvider;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class VerilogProjectLogicalView implements LogicalViewProvider {

	private final VerilogProject project;
//	private FileChangeListener fileChangeListener;
	VerilogProjectNode root;

	public VerilogProjectLogicalView(VerilogProject project) {
		this.project = project;

//		if (fileChangeListener == null) {
//			this.fileChangeListener = (FileChangeListener) WeakListeners.create(FileChangeListener.class, this, project.getProjectDirectory().getPath());
//			project.getProjectDirectory().addFileChangeListener(fileChangeListener);
//		}
	}

	@Override
	public org.openide.nodes.Node createLogicalView() {
		try {
			FileObject folder = project.getProjectDirectory();
			DataFolder textDataObject = DataFolder.findFolder(folder);
			Node realTextFolderNode = textDataObject.getNodeDelegate();
			root = new VerilogProjectNode(realTextFolderNode, project);

			return root;
		} catch (DataObjectNotFoundException donfe) {
			Exceptions.printStackTrace(donfe);
			return new AbstractNode(Children.LEAF);
		}
	}

	@Override
	public Node findPath(Node root, Object target) {
		return null;
	}

//	@Override
//	public void fileFolderCreated(FileEvent fe) {
//		System.out.println("fileFolderCreated " + fe.getFile().getPath());
//		root.notify();
//	}
//
//	@Override
//	public void fileDataCreated(FileEvent fe) {
//		System.out.println("fileDataCreated " + fe.getFile().getPath());
//		root.notify();
//	}
//
//	@Override
//	public void fileChanged(FileEvent fe) {
//		System.out.println("fileChanged " + fe.getFile().getPath());
//		root.notify();
//	}
//
//	@Override
//	public void fileDeleted(FileEvent fe) {
//	}
//
//	@Override
//	public void fileRenamed(FileRenameEvent fre) {
//		root.notify();
//	}
//
//	@Override
//	public void fileAttributeChanged(FileAttributeEvent fae) {
//	}
}
