/*
 * Copyright 2021 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.netbeans.verilog.projecttype;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.netbeans.api.project.Project;
import org.netbeans.spi.project.MoveOrRenameOperationImplementation;
import org.openide.filesystems.FileObject;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class RenameOperation implements MoveOrRenameOperationImplementation {

	@Override
	public void notifyRenaming() throws IOException {
	}

	@Override
	public void notifyRenamed(String string) throws IOException {
	}

	@Override
	public void notifyMoving() throws IOException {
	}

	@Override
	public void notifyMoved(Project prjct, File file, String string) throws IOException {
	}

	@Override
	public List<FileObject> getMetadataFiles() {
		List<FileObject> dataFiles = new ArrayList<>();
		return dataFiles;
	}

	@Override
	public List<FileObject> getDataFiles() {
		List<FileObject> dataFiles = new ArrayList<>();
		return dataFiles;

	}
}
