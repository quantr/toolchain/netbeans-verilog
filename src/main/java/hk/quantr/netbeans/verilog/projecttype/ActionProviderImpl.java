/*
 * Copyright 2021 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.netbeans.verilog.projecttype;

import hk.quantr.netbeans.verilog.debugger.VerilogDebugger;
import org.netbeans.spi.project.ActionProvider;
import org.netbeans.spi.project.ui.support.DefaultProjectOperations;
import org.openide.util.Lookup;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class ActionProviderImpl implements ActionProvider {

	private final String[] supported = new String[]{
		ActionProvider.COMMAND_DELETE,
		ActionProvider.COMMAND_COPY,
		ActionProvider.COMMAND_RENAME,
		ActionProvider.COMMAND_MOVE,
//		ActionProvider.COMMAND_DEBUG_SINGLE,
		ActionProvider.COMMAND_DEBUG,
//		ActionProvider.COMMAND_DEBUG_STEP_INTO,
//		ActionProvider.COMMAND_DEBUG_TEST_SINGLE,
		ActionProvider.COMMAND_RUN,
//		ActionProvider.COMMAND_RUN_SINGLE,
		ActionProvider.COMMAND_BUILD,
		ActionProvider.COMMAND_CLEAN,
		ActionProvider.COMMAND_COMPILE_SINGLE
	};

//	private VerilogDebugger debugger;

	VerilogProject verilogProject;

	ActionProviderImpl(VerilogProject verilogProject) {
		this.verilogProject = verilogProject;

//		debugger = DebuggerManager.getDebuggerManager().lookupFirst(null, VerilogDebugger.class);
//		System.out.println("debugger=" + debugger);
	}

	@Override
	public String[] getSupportedActions() {
		return supported;
	}

	@Override
	public void invokeAction(String str, Lookup lookup) throws IllegalArgumentException {
		System.out.println("ActionProviderImpl::invokeAction");
		if (str.equalsIgnoreCase(ActionProvider.COMMAND_DELETE)) {
			DefaultProjectOperations.performDefaultDeleteOperation(verilogProject);
		} else if (str.equalsIgnoreCase(ActionProvider.COMMAND_COPY)) {
			DefaultProjectOperations.performDefaultCopyOperation(verilogProject);
		} else if (str.equalsIgnoreCase(ActionProvider.COMMAND_MOVE)) {
			DefaultProjectOperations.performDefaultMoveOperation(verilogProject);
		} else if (str.equalsIgnoreCase(ActionProvider.COMMAND_RENAME)) {
			DefaultProjectOperations.performDefaultRenameOperation(verilogProject, null);
		} else if (str.equalsIgnoreCase(ActionProvider.COMMAND_DEBUG)) {
			VerilogDebugger.startDebugger();
		}
	}

	@Override
	public boolean isActionEnabled(String command, Lookup lookup) throws IllegalArgumentException {
		return true;
	}
}
