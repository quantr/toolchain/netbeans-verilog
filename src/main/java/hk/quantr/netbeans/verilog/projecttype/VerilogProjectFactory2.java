package hk.quantr.netbeans.verilog.projecttype;

import java.io.IOException;
import org.netbeans.api.project.Project;
import org.netbeans.spi.project.ProjectFactory;
import org.netbeans.spi.project.ProjectState;
import org.openide.filesystems.FileObject;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
@ServiceProvider(service = ProjectFactory.class)
public class VerilogProjectFactory2 implements ProjectFactory {

//	public static final String PROJECT_DIR = "texts";
	@Override
	public boolean isProject(FileObject projectDirectory) {
		//return projectDirectory.getFileObject(PROJECT_DIR) != null;
//		try {
//			String path = projectDirectory.getPath();
//			long total = Arrays.asList(new File(path).list())
//					.stream()
//					.filter(x -> x.contains(".v"))
//					.collect(Collectors.counting());
//			return projectDirectory.getFileObject("Makefile") != null && total > 0;
		return projectDirectory.getFileObject("verilog.hk") != null;
//		} catch (Exception ex) {
//			return false;
//		}
//		return true;
	}

	@Override
	public Project loadProject(FileObject dir, ProjectState state) throws IOException {
		return isProject(dir) ? new VerilogProject(dir, state) : null;
	}

	@Override
	public void saveProject(final Project project) throws IOException, ClassCastException {
//		FileObject projectRoot = project.getProjectDirectory();
//		if (projectRoot.getFileObject(".") == null) {
//			throw new IOException("Project dir " + projectRoot.getPath()
//					+ " deleted,"
//					+ " cannot save project");
//		}
//		((VerilogProject) project).getVerilogFolder(true);
	}
}
