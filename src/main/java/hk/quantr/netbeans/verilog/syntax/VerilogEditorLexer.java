package hk.quantr.netbeans.verilog.syntax;

import hk.quantr.netbeans.ModuleLib;
import hk.quantr.verilogcompiler.antlr.VerilogLexer;
import java.io.IOException;
import org.netbeans.api.lexer.PartType;
import org.netbeans.api.lexer.Token;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;

public class VerilogEditorLexer implements Lexer<VerilogTokenId> {

	private LexerRestartInfo<VerilogTokenId> info;
	private VerilogLexer lexer;

	public VerilogEditorLexer(LexerRestartInfo<VerilogTokenId> info) throws IOException {
		this.info = info;
		VerilogCharStream charStream = new VerilogCharStream(info.input(), "VerilogEditor");
		lexer = new VerilogLexer(charStream);
	}

	@Override
	public Token<VerilogTokenId> nextToken() {
		try {
			org.antlr.v4.runtime.Token token = lexer.nextToken();
			if (token.getType() != VerilogLexer.EOF) {
				String tokenName = VerilogLanguageHierarchy.getToken(token.getType()).name().replaceAll("'", "");

				if (ModuleLib.isDarkTheme()) {
					VerilogTokenId darkTokenId = VerilogLanguageHierarchy.getToken("DARK_" + tokenName);
					return info.tokenFactory().createToken(darkTokenId);
				} else {
					VerilogTokenId tokenId = VerilogLanguageHierarchy.getToken(tokenName);
//				ModuleLib.log(">>" + tokenId.name() + ", " + token.getText());
					return info.tokenFactory().createToken(tokenId);
				}
			}
		} catch (Exception ex) {
		}

		if (info.input().readLength() > 0) {
			VerilogTokenId dummpToken = VerilogLanguageHierarchy.getToken(VerilogLexer.COMMENT);
			return info.tokenFactory().createToken(dummpToken, info.input().readLength(), PartType.MIDDLE);
		}
		return null;
	}

	@Override
	public Object state() {
		return null;
	}

	@Override
	public void release() {
	}

}
