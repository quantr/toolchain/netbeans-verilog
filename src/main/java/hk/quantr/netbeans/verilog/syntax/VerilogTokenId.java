package hk.quantr.netbeans.verilog.syntax;

import org.netbeans.api.lexer.Language;
import org.netbeans.api.lexer.TokenId;

public class VerilogTokenId implements TokenId {

	private static final Language<VerilogTokenId> language = new VerilogLanguageHierarchy().language();
	public String name;
	private final String primaryCategory;
	private final int id;

	public VerilogTokenId(String name, String primaryCategory, int id) {
		this.name = name;
		this.primaryCategory = primaryCategory;
		this.id = id;
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public int ordinal() {
		return id;
	}

	@Override
	public String primaryCategory() {
		return primaryCategory;
	}

	public static final Language<VerilogTokenId> getLanguage() {
		return language;
	}

	public String toString() {
		return id + ", " + name + ", " + primaryCategory;
	}
}
