package hk.quantr.netbeans.verilog.syntax;

import hk.quantr.netbeans.ModuleLib;
import hk.quantr.verilogcompiler.antlr.VerilogLexer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.antlr.v4.runtime.RuntimeMetaData;
import org.netbeans.spi.lexer.LanguageHierarchy;
import org.netbeans.spi.lexer.Lexer;
import org.netbeans.spi.lexer.LexerRestartInfo;
import org.openide.util.Exceptions;

public class VerilogLanguageHierarchy extends LanguageHierarchy<VerilogTokenId> {

	private final static List<VerilogTokenId> tokens = new ArrayList<>();
	private final static Map<Integer, VerilogTokenId> idToTokens = new HashMap<>();

	static {
		System.out.println("realTimeCompile, antlr version=" + RuntimeMetaData.VERSION);
		int index = 0;
		for (int x = 0; x <= VerilogLexer.VOCABULARY.getMaxTokenType(); x++) {
			String name = VerilogLexer.VOCABULARY.getDisplayName(x);
			if (name == null) {
				name = "INVALID" + x;
//				name = VerilogLexer.ruleNames[x];
			}
			name = name.replaceAll("'", "");
			ModuleLib.log("name=" + name);
			VerilogTokenId token = new VerilogTokenId(name, name, index);
			tokens.add(token);
			idToTokens.put(index, token);
			index++;
		}

		for (int x = 0; x <= VerilogLexer.VOCABULARY.getMaxTokenType(); x++) {
			String name = VerilogLexer.VOCABULARY.getDisplayName(x);
			if (name == null) {
				name = "INVALID" + x;
//				name = VerilogLexer.ruleNames[x];
			}
			name = name.replaceAll("'", "");

			VerilogTokenId token = new VerilogTokenId("DARK_" + name, "DARK_" + name, index);
			tokens.add(token);
			idToTokens.put(index, token);
			index++;
		}
	}

	public static synchronized VerilogTokenId getToken(int id) {
		return idToTokens.get(id);
	}

	public static synchronized VerilogTokenId getToken(String name) {
		for (Map.Entry<Integer, VerilogTokenId> entry : idToTokens.entrySet()) {
			if (entry.getValue().name().equals(name)) {
				return entry.getValue();
			}
		}
		return null;
	}

	@Override
	protected synchronized Collection<VerilogTokenId> createTokenIds() {
		return tokens;
	}

	@Override
	protected synchronized Lexer<VerilogTokenId> createLexer(LexerRestartInfo<VerilogTokenId> info) {
		try {
			return new VerilogEditorLexer(info);
		} catch (IOException ex) {
			Exceptions.printStackTrace(ex);
			return null;
		}
	}

	@Override
	protected String mimeType() {
		return "text/x-verilog";
	}
}
