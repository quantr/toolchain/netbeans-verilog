/*
 * Copyright 2022 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.netbeans.verilog.quantrprofiling;

import java.io.File;
import java.math.BigInteger;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class ProfilingData {

	public File file;
	public int lineNo;
	public int time;
	public String wire;
	public BigInteger value;

	public ProfilingData(File file, int lineNo, int time, String wire, BigInteger value) {
		this.file = file;
		this.lineNo = lineNo;
		this.time = time;
		this.wire = wire;
		this.value = value;
	}

	@Override
	public String toString() {
		return "ProfilingData{" + "file=" + file + ", lineNo=" + lineNo + ", time=" + time + ", wire=" + wire + ", value=" + value + '}';
	}
}
