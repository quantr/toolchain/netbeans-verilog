/*
 * Copyright 2022 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.netbeans.verilog.quantrprofiling;

import hk.quantr.javalib.CommonLib;
import hk.quantr.netbeans.verilog.VerilogDataObject;
import hk.quantr.netbeans.verilog.debugger.highlight.ProfilingSideBarPanel;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import javax.swing.JPanel;
import org.apache.commons.io.IOUtils;
import org.openide.loaders.DataObject;
import org.openide.util.Exceptions;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class QuantrProfiling {

    public File file;
    public ArrayList<ProfilingData> data = new ArrayList<>();
    public HashSet<File> files = new HashSet<>();
    public HashMap<Integer, ArrayList<ProfilingData>> dataWithLineNo = new HashMap<>();
    public HashMap<Integer, ArrayList<ProfilingData>> dataWithTimes = new HashMap<>();
    public HashSet<Integer> times = new HashSet<>();
    public HashSet<JPanel> sideBars = new HashSet<>();
    public int currentTimeIndex = 0;
    public int currentTime = -1;
    public boolean start = false;

    private static QuantrProfiling instance;

    public static QuantrProfiling getInstance() {
        if (instance == null) {
            instance = new QuantrProfiling();
        }
        return instance;
    }

    public void read(File file) {
        try {
            currentTimeIndex = 0;
            currentTime = -1;
            start = false;
            data.clear();
            files.clear();
            dataWithLineNo.clear();
            dataWithTimes.clear();
            times.clear();

            this.file = file;
            if (file.exists()) {
                System.out.println("reading " + file.getAbsolutePath());
                int lastLineNo = Integer.MIN_VALUE;
                int lastTimes = Integer.MIN_VALUE;
                for (String line : IOUtils.toString(new FileInputStream(file), "utf-8").split("\n")) {
                    line = line.trim();
                    System.out.println(line);
                    String cols[] = line.split(",");
                    String path = cols[0].split("=")[1];
                    int lineNo = Integer.parseInt(cols[1].split("=")[1]);
                    int time = Integer.parseInt(cols[2].split("=")[1]);
                    String wire = cols[3].split("=")[0];
                    BigInteger value = CommonLib.string2BigInteger(cols[3].split("=")[1]);

                    if (lineNo != lastLineNo) {
                        dataWithLineNo.put(lineNo, new ArrayList<>());
                        lastLineNo = lineNo;
                    }
                    if (time != lastTimes) {
                        dataWithTimes.put(time, new ArrayList<>());
                        lastTimes = time;
                    }
                    times.add(time);
                    dataWithLineNo.get(lineNo).add(new ProfilingData(new File(path), lineNo, time, wire, value));
                    dataWithTimes.get(time).add(new ProfilingData(new File(path), lineNo, time, wire, value));
                    files.add(new File(path));
                }

                if (!times.isEmpty()) {
                    currentTime = (int) times.toArray()[currentTimeIndex];
                }
                start = true;
                updateAllSideBars();
            }
        } catch (FileNotFoundException ex) {
            Exceptions.printStackTrace(ex);
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }

    }

    public void next() {
        if (currentTimeIndex < times.size() - 1) {
            currentTimeIndex++;
            currentTime = (int) times.toArray()[currentTimeIndex];
            updateAllSideBars();

            Set<TopComponent> set = WindowManager.getDefault().getRegistry().getOpened();
            for (TopComponent com : set) {
                DataObject d = com.getLookup().lookup(VerilogDataObject.class);
                if (d != null) {
                    com.repaint();
                }
            }

//            TopComponent activeTC = TopComponent.getRegistry().getActivated();
//            Collection<DataObject> c = (Collection<DataObject>) activeTC.getLookup().lookupAll(DataObject.class);
//            System.out.println("c=" + c.size());
//            for (DataObject dataObject : c) {
//                System.out.println("p=" + dataObject.getPrimaryFile().getPath());
//            }
        } else {
            System.out.println("QuantrProfiling::next() failed");
        }
    }

    public void updateAllSideBars() {
        System.out.println("            updateAllPanels()=" + sideBars.size());
        for (JPanel temp : sideBars) {
//			System.out.println("    updating " + temp);
            temp.revalidate();
            temp.repaint();
        }
    }

    public void addSideBar(ProfilingSideBarPanel panel) {
        sideBars.add(panel);
    }

    public ArrayList<ProfilingData> currentData() {
        return dataWithTimes.get(currentTime);
    }

    public void stop() {
        start = false;
        updateAllSideBars();
    }
}
