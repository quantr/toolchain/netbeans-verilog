package hk.quantr.netbeans.verilog.hyperlink;

import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.EditorRegistry;
import org.netbeans.api.editor.mimelookup.MimeRegistration;
import org.netbeans.lib.editor.hyperlink.spi.HyperlinkProvider;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
@MimeRegistration(mimeType = "text/x-verilog", service = HyperlinkProvider.class)
public class VerilogHyperlinkProvider implements HyperlinkProvider {

	@Override
	public boolean isHyperlinkPoint(Document dcmnt, int i) {
//		ModuleLib.log("isHyperlinkPoint " + i);
		return true;
	}

	@Override
	public int[] getHyperlinkSpan(Document dcmnt, int i) {
//		ModuleLib.log("getHyperlinkSpan " + i);

//		if (MyVerilogParser.allTokens.size() == 0) {
//			return null;
//		}
//		Token t = MyVerilogParser.allTokens.get(i);
//		return new int[]{t.getStartIndex(), t.getStopIndex()};
		return new int[]{0, 4};
	}

	@Override
	public void performClickAction(Document dcmnt, int i) {
//		ModuleLib.log("performClickAction " + i);
		JTextComponent jTextComponent = EditorRegistry.lastFocusedComponent();
		jTextComponent.setCaretPosition(100);
	}

}
