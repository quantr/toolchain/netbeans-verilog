@TemplateRegistration(folder = "Verilog", content = "verilogmodule.v", requireProject = false, targetName = "sample")
package hk.quantr.netbeans.verilog;

import org.netbeans.api.templates.TemplateRegistration;
