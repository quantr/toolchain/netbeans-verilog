package hk.quantr.netbeans.verilog.debugger.highlight;

import hk.quantr.netbeans.ModuleLib;
import hk.quantr.netbeans.verilog.quantrprofiling.ProfilingData;
import hk.quantr.netbeans.verilog.quantrprofiling.QuantrProfiling;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.openide.loaders.DataObject;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class ProfilingHighlighter extends DefaultHighlighter {

	private JTextComponent textComponent;

	@Override
	public final void install(final JTextComponent c) {
		super.install(c);
		this.textComponent = c;
	}

	@Override
	public final void deinstall(final JTextComponent c) {
		super.deinstall(c);
		this.textComponent = null;
	}

	public final void paint(final Graphics g) {
		if (QuantrProfiling.getInstance().start) {

			Document document = textComponent.getDocument();
			DataObject dataObject = NbEditorUtilities.getDataObject(document);
			HashMap<Integer, ImmutablePair<Integer, Double>> map = ModuleLib.getLineY(g, textComponent);

			g.setFont(textComponent.getFont());

			Graphics2D g2 = (Graphics2D) g;
			g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
			FontMetrics metrics = g.getFontMetrics(g.getFont());
			int originalFontHeight = metrics.getHeight();

			ArrayList<ProfilingData> data = QuantrProfiling.getInstance().currentData();
			if (data != null) {
//                g.setColor(Color.decode("#faedcd"));
				HashMap<Integer, String> count = new HashMap<>();
				Font oldFont = g.getFont();
				for (ProfilingData temp : data) {
					File realFile = ModuleLib.getRealFile(temp.file);
					if (realFile.equals(new File(dataObject.getPrimaryFile().getPath()))) {
						ImmutablePair<Integer, Double> pair = map.get(temp.lineNo);
						if (pair != null && pair.getLeft() != null && pair.getRight() != null) {
							String text = textComponent.getText().split("\n")[temp.lineNo - 1];

							g.setColor(Color.decode("#eee4e1"));
							g.fillRect(0, pair.getLeft(), textComponent.getWidth(), pair.getRight().intValue());
							g.setColor(Color.black);
							String c = count.get(temp.lineNo);
							if (c == null) {
								c = "";
							}
							c += temp.wire + " = 0x" + temp.value.toString(16) + ", ";
							Integer zoom = (Integer) textComponent.getClientProperty("text-zoom");
							if (zoom != null) {
								g.setFont(oldFont.deriveFont((float) oldFont.getSize() + zoom));
							}

							int newFontHeight = g.getFontMetrics(g.getFont()).getHeight();
							float zoomRatio = (float) newFontHeight / originalFontHeight;

							g.drawString(c, (int) ((50 + metrics.stringWidth(text)) * zoomRatio), (int) (pair.getLeft() + pair.getRight() - (metrics.getDescent() * zoomRatio)));
							count.put(temp.lineNo, c);
						}
					}
				}
			}
		}
	}

}
