/*
 * Copyright 2019 junichi11.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.netbeans.verilog.debugger.highlight;

import hk.quantr.netbeans.ModuleLib;
import hk.quantr.netbeans.verilog.quantrprofiling.ProfilingData;
import hk.quantr.netbeans.verilog.quantrprofiling.QuantrProfiling;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JPanel;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.openide.loaders.DataObject;

public final class ProfilingSideBarPanel extends JPanel {

	private final JTextComponent textComponent;

	private static final int DEFAULT_WIDTH = 50;

	public ProfilingSideBarPanel(JTextComponent editor) {
		this.textComponent = editor;
		QuantrProfiling.getInstance().addSideBar(this);
	}

	@Override
	public Dimension getPreferredSize() {
		if (QuantrProfiling.getInstance().start) {
			Dimension dim = textComponent.getSize();
			dim.width = DEFAULT_WIDTH;
			return dim;
		} else {
			return new Dimension(0, 0);
		}
	}

	@Override
	public void paint(final Graphics g) {
		super.paint(g);
		if (QuantrProfiling.getInstance().start) {

			Document document = textComponent.getDocument();
			DataObject dataObject = NbEditorUtilities.getDataObject(document);
			HashMap<Integer, ImmutablePair<Integer, Double>> map = ModuleLib.getLineY(g, textComponent);

			g.setFont(textComponent.getFont());

			Graphics2D g2 = (Graphics2D) g;
			g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
			FontMetrics metrics = g.getFontMetrics(g.getFont());
			int originalFontHeight = metrics.getHeight();

			ArrayList<ProfilingData> data = QuantrProfiling.getInstance().currentData();
			if (data != null) {
				Font oldFont = g.getFont();
				for (ProfilingData temp : data) {
					File realFile = ModuleLib.getRealFile(temp.file);
					if (realFile.equals(new File(dataObject.getPrimaryFile().getPath()))) {
						ImmutablePair<Integer, Double> pair = map.get(temp.lineNo);
						if (pair != null && pair.getLeft() != null && pair.getRight() != null) {
							g.setColor(Color.decode("#eee4e1"));
							g.fillRect(0, pair.getLeft(), textComponent.getWidth(), pair.getRight().intValue());

//                            Rectangle2D bounds = g.getFontMetrics().getStringBounds("" + QuantrProfiling.getInstance().currentTime, g);
							int y = pair.getLeft();
							int lineHeight = pair.right.intValue();

							int newFontHeight = g.getFontMetrics(g.getFont()).getHeight();
							float zoomRatio = (float) newFontHeight / originalFontHeight;

							Integer zoom = (Integer) textComponent.getClientProperty("text-zoom");
							if (zoom != null) {
								g.setFont(oldFont.deriveFont((float) oldFont.getSize() + zoom));
							}

							g.setColor(Color.black);
							g.drawString("" + QuantrProfiling.getInstance().currentTime, 10, (int) (y + lineHeight - (metrics.getDescent() * zoomRatio)));
//                            g.setColor(Color.blue);
//                            g.drawRect(10, y + margin, metrics.stringWidth("" + QuantrProfiling.getInstance().currentTime), metrics.getHeight());
						}
					}
				}
			}
		}

	}

}
