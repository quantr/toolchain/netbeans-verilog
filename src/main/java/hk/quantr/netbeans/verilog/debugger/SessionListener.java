/*
 * Copyright 2022 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.netbeans.verilog.debugger;

import org.netbeans.api.debugger.DebuggerManagerAdapter;
import org.netbeans.api.debugger.Session;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class SessionListener extends DebuggerManagerAdapter{

    @Override
    public void sessionAdded( Session session ) {
        super.sessionAdded(session);
    }

    @Override
    public void sessionRemoved( Session session ) {
        super.sessionRemoved(session);
    }

}
