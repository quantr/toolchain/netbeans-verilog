/*
 * Copyright 2022 peter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.netbeans.verilog.debugger;

import hk.quantr.netbeans.ModuleLib;
import hk.quantr.netbeans.verilog.quantrprofiling.QuantrProfiling;
import java.io.File;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.swing.SwingUtilities;
import org.netbeans.api.debugger.DebuggerInfo;
import org.netbeans.api.debugger.DebuggerManager;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.spi.debugger.ActionsProviderSupport;
import org.netbeans.spi.debugger.ContextProvider;
import org.netbeans.spi.debugger.DebuggerEngineProvider;
import org.netbeans.spi.debugger.SessionProvider;
import org.openide.cookies.LineCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.text.Line;
import org.openide.windows.TopComponent;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class VerilogDebugger extends ActionsProviderSupport {

    public static final Object ACTION_STEP_OVER = "stepOver";
    public static final Object ACTION_STEP_BACK_OVER = "stepBackOver";
    public static final Object ACTION_RUN_INTO_METHOD = "runIntoMethod";
    public static final Object ACTION_STEP_INTO = "stepInto";
    public static final Object ACTION_STEP_BACK_INTO = "stepBackInto";
    public static final Object ACTION_STEP_OUT = "stepOut";
    public static final Object ACTION_STEP_OPERATION = "stepOperation";
    public static final Object ACTION_CONTINUE = "continue";
    public static final Object ACTION_REWIND = "rewind";
    public static final Object ACTION_START = "start";
    public static final Object ACTION_KILL = "kill";
    public static final Object ACTION_MAKE_CALLER_CURRENT = "makeCallerCurrent";
    public static final Object ACTION_MAKE_CALLEE_CURRENT = "makeCalleeCurrent";
    public static final Object ACTION_PAUSE = "pause";
    public static final Object ACTION_RUN_TO_CURSOR = "runToCursor";
    public static final Object ACTION_RUN_BACK_TO_CURSOR = "runBackToCursor";
    public static final Object ACTION_POP_TOPMOST_CALL = "popTopmostCall";
    public static final Object ACTION_FIX = "fix";
    public static final Object ACTION_RESTART = "restart";
    public static final Object ACTION_TOGGLE_BREAKPOINT = "toggleBreakpoint";

    private VerilogEngineProvider engineProvider;
    private static final Set actions = new HashSet();

    public static final String VERILOG_DEBUGGER_INFO = "VerilogDebuggerInfo";
    public static final String VERILOG_SESSION = "VerilogSession";

    public VerilogDebugger(ContextProvider contextProvider) {
        engineProvider = (VerilogEngineProvider) contextProvider.lookupFirst(null, DebuggerEngineProvider.class);
        for (Iterator it = actions.iterator(); it.hasNext();) {
            setEnabled(it.next(), true);
        }
    }

    static {
        actions.add(ACTION_RUN_BACK_TO_CURSOR);
        actions.add(ACTION_STEP_BACK_INTO);
        actions.add(ACTION_STEP_BACK_OVER);
        actions.add(ACTION_REWIND);
        actions.add(ACTION_KILL);
        actions.add(ACTION_PAUSE);
        actions.add(ACTION_CONTINUE);
        actions.add(ACTION_START);
        actions.add(ACTION_STEP_INTO);
        actions.add(ACTION_STEP_OVER);
        actions.add(ACTION_RUN_TO_CURSOR);
    }

    public static void startDebugger() {
        DebuggerManager manager = DebuggerManager.getDebuggerManager();
        DebuggerInfo info = DebuggerInfo.create(VERILOG_DEBUGGER_INFO,
                new Object[]{
                    new SessionProvider() {

                @Override
                public String getSessionName() {
                    return "Verilog Program";
                }

                @Override
                public String getLocationName() {
                    return "localhost";
                }

                @Override
                public String getTypeID() {
                    return VERILOG_SESSION;
                }

                @Override
                public Object[] getServices() {
                    return new Object[]{};
                }
            }, null
                });

        manager.startDebugging(info);
    }

    @Override
    public void doAction(Object action) {
        System.out.println("VerilogDebugger::doAction=" + action);
        if (action == ACTION_RUN_BACK_TO_CURSOR) {
        } else if (action == ACTION_STEP_BACK_INTO) {
        } else if (action == ACTION_STEP_BACK_OVER) {
        } else if (action == ACTION_REWIND) {
        } else if (action == ACTION_KILL) {
            QuantrProfiling.getInstance().stop();
            engineProvider.getDestructor().killEngine();
        } else if (action == ACTION_PAUSE) {
        } else if (action == ACTION_CONTINUE) {
        } else if (action == ACTION_START) {
            TopComponent activeC = TopComponent.getRegistry().getActivated();
            FileObject dataLookup = activeC.getLookup().lookup(FileObject.class);
            if (dataLookup == null) {
                return;
            }
            Project pro = FileOwnerQuery.getOwner(dataLookup);
            String path = pro.getProjectDirectory().getPath();
            File file = new File(path + File.separator + "quantr.profiling");
            QuantrProfiling.getInstance().read(file);

            for (File temp : QuantrProfiling.getInstance().files) {
                try {
                    File realFile = ModuleLib.getRealFile(temp);
                    LineCookie lc = DataObject.find(FileUtil.toFileObject(realFile)).getLookup().lookup(LineCookie.class);
                    if (lc != null) {
                        int lineNumber = 2;
                        int colNumber = 3;
                        Line line = lc.getLineSet().getOriginal(lineNumber);
                        SwingUtilities.invokeLater(() -> line.show(Line.ShowOpenType.OPEN, Line.ShowVisibilityType.NONE, colNumber));
                    }
                } catch (DataObjectNotFoundException ex) {
                    ex.printStackTrace();
                }
            }
        } else if (action == ACTION_STEP_INTO) {
        } else if (action == ACTION_STEP_OVER) {
            QuantrProfiling.getInstance().next();
        } else if (action == ACTION_RUN_TO_CURSOR) {
        }
    }

    @Override
    public Set getActions() {
        return actions;
    }
}
