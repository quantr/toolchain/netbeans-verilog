package hk.quantr.netbeans.verilog.errorhighlight;

import hk.quantr.netbeans.verilog.parser.MyParseResult;
import hk.quantr.netbeans.ModuleLib;
import hk.quantr.netbeans.verilog.parser.MyVerilogErrorListener;
import java.util.ArrayList;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.modules.parsing.spi.Parser.Result;
import org.netbeans.modules.parsing.spi.ParserResultTask;
import org.netbeans.modules.parsing.spi.Scheduler;
import org.netbeans.modules.parsing.spi.SchedulerEvent;
import org.netbeans.spi.editor.hints.ErrorDescription;
import org.netbeans.spi.editor.hints.ErrorDescriptionFactory;
import org.netbeans.spi.editor.hints.HintsController;
import org.netbeans.spi.editor.hints.Severity;
import org.openide.util.Exceptions;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class ErrorHighlightingTask extends ParserResultTask {

	public static ArrayList<ErrorInfo> errorInfos = new ArrayList<>();

	@Override
	public void run(Result result, SchedulerEvent event) {
		ModuleLib.log("ErrorHighlightingTask");
		MyVerilogErrorListener listener = ((MyParseResult) result).errorListerner;
		Document document = result.getSnapshot().getSource().getDocument(false);
		ArrayList<ErrorDescription> errors = new ArrayList<>();
		for (ErrorInfo errorInfo : listener.errorInfos) {
			try {
				ErrorDescription errorDescription = ErrorDescriptionFactory.createErrorDescription(
						Severity.ERROR,
						errorInfo.message,
						document,
						document.createPosition(errorInfo.offsetStart),
						document.createPosition(errorInfo.offsetEnd + 1)
				);
				ModuleLib.log("errorDescription=" + errorDescription);
				errors.add(errorDescription);
			} catch (BadLocationException ex) {
				Exceptions.printStackTrace(ex);
			}
		}
		HintsController.setErrors(document, "verilog", errors);
	}

	@Override
	public int getPriority() {
		return 100;
	}

	@Override
	public Class getSchedulerClass() {
		return Scheduler.EDITOR_SENSITIVE_TASK_SCHEDULER;
	}

	@Override
	public void cancel() {
	}

}
