package hk.quantr.netbeans.verilog.parser;

import java.util.Collection;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.ParserFactory;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class MyVerilogParserFactory extends ParserFactory {

	@Override
	public org.netbeans.modules.parsing.spi.Parser createParser(Collection<Snapshot> clctn) {
		return new MyVerilogParser();
	}

}
