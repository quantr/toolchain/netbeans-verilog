package hk.quantr.netbeans.verilog.parser;

import hk.quantr.verilogcompiler.antlr.VerilogParserBaseListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import org.antlr.v4.runtime.Parser;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class MyVerilogParserListener extends VerilogParserBaseListener {

	private final Map<String, Integer> rmap = new HashMap<>();

	public MyVerilogParserListener(Parser parser) {
		rmap.clear();
		rmap.putAll(parser.getRuleIndexMap());
	}

//	@Override
//	public void enterEveryRule(ParserRuleContext ctx) {
//		String ruleName = getRuleByKey(ctx.getRuleIndex());
//		System.out.println("enterEveryRule " + ctx.getText() + "\t" + ruleName);
//	}

//	@Override
//	public void exitEveryRule(ParserRuleContext ctx) {
//		String ruleName = getRuleByKey(ctx.getRuleIndex());
//		System.out.println("exitEveryRule " + ctx.getText() + "\t" + ruleName);
//	}

	public String getRuleByKey(int key) {
		return rmap.entrySet().stream()
				.filter(e -> Objects.equals(e.getValue(), key))
				.map(Map.Entry::getKey)
				.findFirst()
				.orElse(null);
	}
}
