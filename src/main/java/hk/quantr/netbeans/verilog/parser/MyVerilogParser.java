package hk.quantr.netbeans.verilog.parser;

import hk.quantr.netbeans.ModuleLib;
import hk.quantr.verilogcompiler.VerilogHelper;
import hk.quantr.verilogcompiler.antlr.VerilogLexer;
import hk.quantr.verilogcompiler.antlr.VerilogParser;
import hk.quantr.verilogcompiler.macro.ParseStructure;
import java.io.File;
import javax.swing.event.ChangeListener;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Task;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.SourceModificationEvent;
import org.openide.util.Exceptions;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class MyVerilogParser extends Parser {

	private Snapshot snapshot;
	private MyParseResult result;

	@Override
	public void parse(Snapshot snapshot, Task task, SourceModificationEvent sme) throws ParseException {
		try {
			ModuleLib.log("VerilogParser::parse()");
			this.snapshot = snapshot;

			String content = snapshot.getText().toString();
			File file = new File(snapshot.getSource().getFileObject().getPath());
			File folder = new File(snapshot.getSource().getFileObject().getParent().getPath());
			System.out.println("snapshot.getSource().getFileObject().getPath()=" + file);
			System.out.println("folder=" + folder.getAbsolutePath());
			ParseStructure ps = new ParseStructure();
//			String sourceContent = VerilogHelper.getContentReplacedWithMacro(0, content, VerilogHelper.scanMacroDefinitions(folder), ps);
			String sourceContent = VerilogHelper.getContentReplacedWithMacro(1, file, content, VerilogHelper.scanMacroDefinitions(file, folder), ps);

			MyVerilogErrorListener errorListerner = new MyVerilogErrorListener();
			VerilogLexer lexer = new VerilogLexer(CharStreams.fromString(sourceContent));
			lexer.addErrorListener(errorListerner);
			CommonTokenStream tokenStream = new CommonTokenStream(lexer);
			VerilogParser parser = new VerilogParser(tokenStream);
			parser.addErrorListener(errorListerner);
			VerilogParser.Source_textContext context = parser.source_text();
			ParseTreeWalker walker = new ParseTreeWalker();
			MyVerilogParserListener listener = new MyVerilogParserListener(parser);
			walker.walk(listener, context);

			result = new MyParseResult(snapshot, listener, errorListerner);
		} catch (Exception ex) {
			Exceptions.printStackTrace(ex);
		}
	}

	@Override
	public Result getResult(Task task) throws ParseException {
		return result;
	}

	@Override
	public void addChangeListener(ChangeListener cl) {
	}

	@Override
	public void removeChangeListener(ChangeListener cl) {
	}

}
