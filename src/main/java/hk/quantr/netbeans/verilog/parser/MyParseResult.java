package hk.quantr.netbeans.verilog.parser;

import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.Parser.Result;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class MyParseResult extends Result {

	public MyVerilogParserListener listener;
	public MyVerilogErrorListener errorListerner;

	public MyParseResult(Snapshot _snapshot, MyVerilogParserListener listener, MyVerilogErrorListener errorListerner) {
		super(_snapshot);
		this.listener = listener;
		this.errorListerner = errorListerner;
	}

	@Override
	protected void invalidate() {

	}

}
