package hk.quantr.netbeans.verilog.window.projectoutline;

import hk.quantr.verilogcompiler.antlr.VerilogParser;
import hk.quantr.verilogcompiler.antlr.VerilogParser.Port_identifierContext;
import hk.quantr.verilogcompiler.antlr.VerilogParserBaseListener;
import hk.quantr.verilogcompiler.structure.Port;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class ProjectOutlineVerilogListener extends VerilogParserBaseListener {

	ArrayList<Port> inputs = new ArrayList<>();
	ArrayList<Port> outputs = new ArrayList<>();
	String moduleName;

	@Override
	public void exitInput_declaration(VerilogParser.Input_declarationContext ctx) {
		String type = ctx.net_type() == null ? null : ctx.net_type().getText();
		String range = ctx.range_() == null ? null : ctx.range_().getText();

		List<Port_identifierContext> list = ctx.list_of_port_identifiers().port_identifier();
		for (Port_identifierContext portCtx : list) {
			inputs.add(new Port("input", type, range, portCtx.getText()));
		}
	}

	@Override
	public void exitOutput_declaration(VerilogParser.Output_declarationContext ctx) {
		String type = ctx.net_type() == null ? null : ctx.net_type().getText();
		String range = ctx.range_() == null ? null : ctx.range_().getText();

		List<Port_identifierContext> list = ctx.list_of_port_identifiers().port_identifier();
		for (Port_identifierContext portCtx : list) {
			outputs.add(new Port("output", type, range, portCtx.getText()));
		}
	}

	@Override
	public void exitModule_declaration(VerilogParser.Module_declarationContext ctx) {
		moduleName = ctx.module_identifier().getText();
	}

}
