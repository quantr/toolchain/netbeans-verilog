package hk.quantr.netbeans.verilog.window.projectoutline;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import javax.swing.ImageIcon;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.TreeCellRenderer;

public class ProjectOutlineTreeRenderer extends JPanel implements TreeCellRenderer {

	Color color = UIManager.getColor("Tree.foreground");
	Color backgroundColor = UIManager.getColor("Tree.background");
	Color selectedColor = UIManager.getColor("Tree.selectionForeground");
	Color selectedBackgroundColor = UIManager.getColor("Tree.selectionBackground");

	ImageIcon moduleIcon = new ImageIcon(getClass().getClassLoader().getResource("hk/quantr/netbeans/verilog/verilog.png"));
	ImageIcon portIcon = new ImageIcon(getClass().getClassLoader().getResource("hk/quantr/netbeans/verilog/port.png"));
	JLabel label = new JLabel();

	public ProjectOutlineTreeRenderer() {
		setLayout(new BorderLayout());
		add(label, BorderLayout.CENTER);
		label.setOpaque(true);
		label.setBorder(new EmptyBorder(10, 10, 10, 10));
	}

	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
		if (value instanceof ProjectTreeNode) {
			ProjectTreeNode node = (ProjectTreeNode) value;
			label.setIcon(moduleIcon);
			label.setText(node.toString());
		} else if (value instanceof ModuleTreeNode) {
			ModuleTreeNode node = (ModuleTreeNode) value;
			label.setIcon(moduleIcon);
			label.setText(node.toString());
		} else if (value instanceof PortTreeNode) {
			PortTreeNode node = (PortTreeNode) value;
			label.setIcon(portIcon);
			label.setText(node.toString());
		} else {
			label.setIcon(null);
			label.setText(value.toString());
		}
		if (hasFocus) {
			label.setForeground(selectedColor);
			label.setBackground(selectedBackgroundColor);
		} else {
			label.setForeground(color);
			label.setBackground(backgroundColor);
		}
		return this;
	}

	@Override
	public Dimension getPreferredSize() {
		Dimension d = label.getPreferredSize();
		return new Dimension(d.width, 25);
	}
}
