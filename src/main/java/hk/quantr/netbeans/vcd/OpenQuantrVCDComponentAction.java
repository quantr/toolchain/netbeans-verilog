/*
 * Copyright The string doesn't match the expected date/time/date-time format. The string to parse was: "2022年1月2日". The expected format was: "MMM d, y".
The nested reason given follows:
Unparseable date: "2022年1月2日"

----
FTL stack trace ("~" means nesting-related):
	- Failed at: ${date?date?string("yyyy")}  [in template "Templates/Licenses/license-apache20.txt" at line 4, column 27]
	- Reached through: #include "${project.licensePath}"  [in template "Templates/NetBeansModuleDevelopment-files/contextAction.java" at line 24, column 1]
---- Peter.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.netbeans.vcd;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import org.openide.loaders.DataObject;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle.Messages;

@ActionID(
		category = "Window",
		id = "hk.quantr.netbeans.verilog.vcd.OpenQuantrVCDComponentAction"
)
@ActionRegistration(
		iconBase = "hk/quantr/netbeans/vcd/All_Q_Logos_purple.png",
		displayName = "#CTL_OpenQuantrVCDComponentAction"
)
@ActionReference(path = "Loaders/text/quantr+vcd/Actions", position = 1000)
@Messages("CTL_OpenQuantrVCDComponentAction=Open Quantr VCD Component")
public final class OpenQuantrVCDComponentAction implements ActionListener {

	private final DataObject context;

	public OpenQuantrVCDComponentAction(DataObject context) {
		this.context = context;
	}

	@Override
	public void actionPerformed(ActionEvent ev) {
		JOptionPane.showMessageDialog(null, "ppp");
	}
}
