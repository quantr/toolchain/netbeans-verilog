package hk.quantr.netbeans.vcd;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class VCDTableModel implements TableModel {

	@Override
	public int getRowCount() {
		return 10;
	}

	@Override
	public int getColumnCount() {
		return 20;
	}

	@Override
	public String getColumnName(int i) {
		return "h" + String.valueOf(i);
	}

	@Override
	public Class<?> getColumnClass(int i) {
		return String.class;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	@Override
	public Object getValueAt(int x, int y) {
		return y * 20 + x;
	}

	@Override
	public void setValueAt(Object o, int i, int i1) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void addTableModelListener(TableModelListener l) {

	}

	@Override
	public void removeTableModelListener(TableModelListener l) {
		
	}

}
