# Author

Peter <peter@quantr.hk>, System architect from Quantr

# Goal

Verilog plugin for netbeans
- Syntax highlight
- Click to jump to module
- Module hierarchy
- Source formatting , by https://github.com/ericsonj/verilog-format
- Top module graph
- more ...

![245267157_10158417459160208_7217341876476644319_n](https://www.quantr.foundation/wp-content/uploads/2021/10/245267157_10158417459160208_7217341876476644319_n.jpg)

![](https://www.quantr.foundation/wp-content/uploads/2021/10/Screenshot_4.png)


# Architecture

![](https://www.quantr.foundation/wp-content/uploads/2022/06/netbeans-verilog-architecture.drawio.png)

# VCD Component

If you are using verilator and output vcd format, we have a VCD visualize componet

![](https://www.quantr.foundation/wp-content/uploads/2021/11/Screenshot-2021-11-09-at-2.00.41-AM.png)

# Download

http://netbeans.quantr.hk
